"dein Scripts-----------------------------
if &compatible
	set nocompatible               " Be iMproved
endif

" Required: 
set runtimepath+=/Users/neena/.config/nvim/dein-store/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/Users/neena/.config/nvim/dein-store')
	call dein#begin('/Users/neena/.config/nvim/dein-store')

	" Let dein manage dein
	" Required:
	call dein#add('/Users/neena/.config/nvim/dein-store/repos/github.com/Shougo/dein.vim')

	call dein#add('Shougo/deoplete.nvim')
	if !has('nvim')
		call dein#add('roxma/nvim-yarp')
		call dein#add('roxma/vim-hug-neovim-rpc')
	endif
	call dein#add('Shougo/vimproc', {'build': 'make'}) " Necessary for ghcmod-vim

	call dein#add('scrooloose/nerdtree', {'on': 'NERDTreeToggle'})
	call dein#add('ctrlpvim/ctrlp.vim')
	call dein#add('altercation/vim-colors-solarized')
	call dein#add('tpope/vim-fugitive')
	call dein#add('vim-syntastic/syntastic')

	" Clojure
	call dein#add('tpope/vim-fireplace', { 'for': 'clojure' })
	call dein#add('tpope/vim-salve.git', { 'for': 'clojure' })
	call dein#add('tpope/vim-dispatch.git', { 'for': 'clojure' })

	" Go
	call dein#add('zchee/deoplete-go', {'build': 'make'})
	call dein#add('fatih/vim-go', { 'tag': '*' })
	call dein#add('nsf/gocode', { 'do': '/Users/neena/.config/nvim/dein-store/repos/github.com/nsf/gocode/vim/symlink.sh', 'rtp': 'vim' })

	" Haskell
	call dein#add('eagletmt/neco-ghc')
	call dein#add('eagletmt/ghcmod-vim')

	" Rust (disabled)
	" call dein#add('sebastianmarkow/deoplete-rust')

  call dein#add('Shougo/deoplete.nvim')

	" Required:
	call dein#end()
	call dein#save_state()
endif

" Required: (by dein)
filetype plugin indent on
syntax enable
"End dein Scripts-------------------------

" General Stuff Things
let mapleader=","
set expandtab

" Visual
colorscheme desert

if has('gui_running')
	set background=light
else
	set background=dark
endif

" General Mappings
nnoremap <C-s> :w<CR>
nnoremap <Leader>b :CtrlPBuffer<CR>
nnoremap <Leader>m :CtrlPMRUFiles<CR>

" General plugins
" Autocomplete with deoplete
let g:deoplete#enable_at_startup = 1

" Syntax checking with Syntastic
noremap <Leader>s :SyntasticToggleMode<CR>

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

" Go
autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4


nnoremap <C-r> :GoTestFunc<CR>
nnoremap <C-R> :GoTest<CR>

let g:go_fmt_command = "goimports"
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_metalinter_autosave = 0

" Haskell
let g:necoghc_enable_detailed_browse = 1
let g:necoghc_debug = 1
let g:necoghc_use_stack = 1
noremap <silent> tw :GhcModTypeInsert<CR>
noremap <silent> ts :GhcModSplitFunCase<CR>
noremap <silent> tq :GhcModType<CR>
noremap <silent> te :GhcModTypeClear<CR>

" Rust
" let g:deoplete#sources#rust#racer_binary='/Users/neena/work/neena/rust/racer'
" let g:deoplete#sources#rust#rust_source_path='/Users/neena/work/neena/rust/src'
" let g:deoplete#sources#rust#documentation_max_height=20
"

" Ruby
autocmd Filetype ruby set softtabstop=2
autocmd Filetype ruby set sw=2
autocmd Filetype ruby set ts=2

let g:deoplete#enable_at_startup = 1

